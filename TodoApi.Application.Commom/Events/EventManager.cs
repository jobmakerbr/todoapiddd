﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Domain.Commom.Contracts;

namespace TodoApi.Domain.Commom.Events
{
    public class EventManager : IEventManager
    {
        private Dictionary<Type, List<IHandle<IDomainEvent>>> _handlers;

        public void Register<T>(IHandle<T> eventHandler) where T : IDomainEvent
        {
            if (!_handlers[typeof(T)].Contains(((IHandle<IDomainEvent>)eventHandler)))
            {
                _handlers[typeof(T)].Add(((IHandle<IDomainEvent>)eventHandler));
            }
        }

        public void Raise<T>(T domainEvent) where T : IDomainEvent
        {
            foreach (IHandle<T> handler in _handlers[domainEvent.GetType()])
            {
                handler.Handle(domainEvent);
            }
        }
    }
}
