﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApi.Domain.Commom.Contracts
{
    public interface IHandle<T> where T : IDomainEvent
    {
        void Handle(T argument);
    }
}
