﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApi.Domain.Commom.Contracts
{
    public interface IEventManager
    {
        void Register<T>(IHandle<T> eventHandler) where T : IDomainEvent;
        void Raise<T>(T domainEvent) where T : IDomainEvent;
    }
}
