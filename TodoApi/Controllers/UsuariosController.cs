﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Domain.Usuarios.Contracts.Services;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly IServiceUsuarios serviceUsuarios;

        public UsuariosController(IServiceUsuarios serviceUsuarios)
        {
            this.serviceUsuarios = serviceUsuarios;
        }
        // POST api/values
        [HttpPost("login/{login}/senha/{senha}")]
        public IActionResult Post(string login, string senha)
        {
            var resp = this.serviceUsuarios.Login(new Domain.Usuarios.Dtos.LoginQuery { Login = login, Senha = senha });

            if (resp.IsValid)
            {
                return Ok();
            }

            return BadRequest(string.Join(",", resp.Messages));
        }

        // PUT api/values/5
        [HttpPut("usuario/{id}/equipe/{idequipe}")]
        public IActionResult AlterarUsuario(string id, string idequpe)
        {
            var resp = this.serviceUsuarios.AlterarGrupo(new Domain.Usuarios.Dtos.Commands.UsuarioGrupoCommand
            {
                IdNovoGrupo = id,
                IdUsuario = idequpe
            });

            if (resp.IsValid)
            {
                return Ok();
            }

            return BadRequest(string.Join(",", resp.Messages));
        }
    }
}
