﻿using System;
using System.Collections.Generic;

namespace TodoApi.CrossCutting
{
    public class NotifyInvalid
    {
        public NotifyInvalid()
        {
            IsValid = true;
        }

        public bool IsValid { get; set; }
        public List<string> Messages { get; private set; }

        public void AddMessage(string message)
        {
            IsValid = false;
            Messages.Add(message);
        }
    }
}
