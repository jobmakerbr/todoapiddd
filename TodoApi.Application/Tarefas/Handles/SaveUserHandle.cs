﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Application.Usuarios.DomainEvents;
using TodoApi.Domain.Commom.Contracts;
using TodoApi.Domain.Tarefas.Contracts.Services;

namespace TodoApi.Application.Tarefas.Handles
{
    public class SaveUserHandle : IHandle<SavedUserEvent>
    {
        private readonly IServiceTarefas serviceTarefas;

        public SaveUserHandle(IServiceTarefas serviceTarefas)
        {
            this.serviceTarefas = serviceTarefas;
        }
        public void Handle(SavedUserEvent argument)
        {
            this.serviceTarefas.AlterarTarefas(new Domain.Tarefas.Dtos.TarefaEquipeCommand {
                IdNovoGrupo = argument.IdNovoGrupo,
                IdUsuario = argument.IdUsuario
            });
        }
    }
}
