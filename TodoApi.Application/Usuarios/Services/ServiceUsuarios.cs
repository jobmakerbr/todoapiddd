﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Application.Usuarios.DomainEvents;
using TodoApi.CrossCutting;
using TodoApi.Domain.Commom.Contracts;
using TodoApi.Domain.Usuarios.Contracts.Repositories;
using TodoApi.Domain.Usuarios.Contracts.Services;
using TodoApi.Domain.Usuarios.Dtos;
using TodoApi.Domain.Usuarios.Dtos.Commands;

namespace TodoApi.Application.Usuarios.Services
{
    public class ServiceUsuarios : IServiceUsuarios
    {
        private readonly IRepositoryUsuario repositoryUsuario;
        private readonly IEventManager eventManager;

        public ServiceUsuarios(IRepositoryUsuario repositoryUsuario, IEventManager eventManager, IServiceProvider services)
        {
            this.repositoryUsuario = repositoryUsuario;
            this.eventManager = eventManager;
            this.eventManager.Register<SavedUserEvent>(services.GetService<IHandle<SavedUserEvent>>());
        }

        public NotifyInvalid AlterarGrupo(UsuarioGrupoCommand usuarioGrupoCommand)
        {
            var notify = new NotifyInvalid();
            var usuario = this.repositoryUsuario.GetById(usuarioGrupoCommand.IdUsuario);
            if (usuario == null)
            {
                notify.AddMessage("Id do usuário não existe!");
            }
            this.repositoryUsuario.Save(usuario);

            var saveEvent = SavedUserEvent.FromCommand(usuarioGrupoCommand);
            this.eventManager.Raise<SavedUserEvent>(saveEvent);

            return notify;
        }

        public NotifyInvalid Login(LoginQuery login)
        {
            var notify = new NotifyInvalid();
            var usuario = this.repositoryUsuario.GetByLogin(login.Login);
            if (usuario == null || usuario.Senha != login.Senha)
            {
                notify.AddMessage("Usuário ou senha inválida!");
            }
            return notify;
        }
    }
}
