﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Domain.Commom.Contracts;
using TodoApi.Domain.Usuarios.Dtos.Commands;

namespace TodoApi.Application.Usuarios.DomainEvents
{
    public class SavedUserEvent : IDomainEvent
    {
        public string IdUsuario { get; set; }
        public string IdNovoGrupo { get; set; }

        public static SavedUserEvent FromCommand(UsuarioGrupoCommand command)
        {
            var saveEvent = new SavedUserEvent();
            saveEvent.IdNovoGrupo = command.IdNovoGrupo;
            saveEvent.IdUsuario = command.IdUsuario;
            return saveEvent;
        }
    }
}
