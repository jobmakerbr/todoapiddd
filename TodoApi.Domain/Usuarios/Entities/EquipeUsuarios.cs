﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.CrossCutting;

namespace TodoApi.Domain.Usuarios.Entities
{
    public class EquipeUsuarios
    {
        public EquipeUsuarios(string nome)
        {
            this.Nome = nome;
        }

        public string Id { get; private set; }
        public string Nome { get; private set; }
        public List<Usuario> Usuarios { get; private set; }


        public NotifyInvalid AddUsuario(Usuario usuario)
        {
            var notify = usuario.Validate();
            if (notify.IsValid)
            {
                this.Usuarios.Add(usuario);
            }
            return notify;
        }
    }
}
