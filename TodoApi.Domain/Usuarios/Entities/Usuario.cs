﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.CrossCutting;
using TodoApi.Domain.Usuarios.ValueObjects;

namespace TodoApi.Domain.Usuarios.Entities
{
    public class Usuario
    {
        public Usuario(string nomecompleto, string login, string senha)
        {
            this.Nome = nomecompleto;
            this.Login = login;
            this.Senha = senha;
        }

        public string Id { get; private set; }
        public NomeCompleto Nome { get; private set; }
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public EquipeUsuarios Equipe { get; private set; }

        public NotifyInvalid Validate()
        {
            var notify = new NotifyInvalid();
            if (!this.Nome.IsValid()) notify.AddMessage("Nome inválido!");

            return notify;
        }
    }
}
