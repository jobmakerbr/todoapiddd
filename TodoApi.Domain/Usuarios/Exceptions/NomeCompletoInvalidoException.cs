﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApi.Domain.Usuarios.Exceptions
{
    public class NomeCompletoInvalidoException : Exception
    {
        public const string MessageShow = "Nome completo inválido!";
        public NomeCompletoInvalidoException() : base(NomeCompletoInvalidoException.MessageShow)
        {
            
        }

        public NomeCompletoInvalidoException(Exception ex) : base(NomeCompletoInvalidoException.MessageShow, ex)
        {

        }
    }
}
