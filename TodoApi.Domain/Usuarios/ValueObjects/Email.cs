﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Domain.Commom.Abstracts;

namespace TodoApi.Domain.Usuarios.ValueObjects
{
    public class Email : ValueObject
    {
        public string EnderecoEmail { get; set; }

        public Email(string email)
        {
            this.EnderecoEmail = email;
        }

        static public implicit operator Email(string value)
        {
            return new Email(value);
        }

        public bool IsValid()
        {
            var valid = true;
            if (!this.EnderecoEmail.Contains("@")) valid = false;

            return valid;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            var valores = new List<object>();
            valores.Add(this.EnderecoEmail);
            return valores;
        }
    }
}
