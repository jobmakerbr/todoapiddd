﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Domain.Commom.Abstracts;
using TodoApi.Domain.Usuarios.Exceptions;

namespace TodoApi.Domain.Usuarios.ValueObjects
{
    public class NomeCompleto : ValueObject
    {
        public string Completo { get; set; }
        public string PrimeiroNome
        {
            get
            {
                if (!this.IsValid()) return null;
                return this.Completo.Split(' ')[0];
            }
        }

        public string UltimoNome
        {
            get
            {
                if (!this.IsValid()) return null;
                var parts = this.Completo.Split(' ');
                return parts[parts.Length-1];
            }
        }

        public NomeCompleto(string nome)
        {
            //Validacões
            Completo = nome;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            var valores = new List<object>();
            valores.Add(this.Completo);
            return valores;
        }

        static public implicit operator NomeCompleto(string value)
        {
            return new NomeCompleto(value);
        }

        public bool IsValid(bool throwException = false)
        {
            var valid = true;
            if (!this.Completo.Contains(" ")) valid = false;

            if (throwException) throw new NomeCompletoInvalidoException();

            return valid;
        }
    }
}
