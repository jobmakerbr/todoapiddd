﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.CrossCutting;

namespace TodoApi.Domain.Usuarios.Dtos
{
    public class LoginQuery
    {
        public string Login { get; set; }
        public string Senha { get; set; }

        public NotifyInvalid Validate()
        {
            var notify = new NotifyInvalid();
            if (string.IsNullOrEmpty(this.Senha)) notify.AddMessage("Senha vazia.");
            if (string.IsNullOrEmpty(this.Login)) notify.AddMessage("Login vazio.");
            return notify;
        }
    }
}
