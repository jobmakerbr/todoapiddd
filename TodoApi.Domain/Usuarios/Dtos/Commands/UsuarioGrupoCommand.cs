﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApi.Domain.Usuarios.Dtos.Commands
{
    public class UsuarioGrupoCommand
    {
        public string IdUsuario { get; set; }
        public string IdNovoGrupo { get; set; }
    }
}
