﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Domain.Usuarios.Entities;

namespace TodoApi.Domain.Usuarios.Contracts.Repositories
{
    public interface IRepositoryUsuario
    {
        Usuario GetById(string id);
        Usuario GetByLogin(string login);
        void Save(Usuario usuario);
    }
}
