﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.CrossCutting;
using TodoApi.Domain.Usuarios.Dtos;
using TodoApi.Domain.Usuarios.Dtos.Commands;

namespace TodoApi.Domain.Usuarios.Contracts.Services
{
    public interface IServiceUsuarios
    {
        NotifyInvalid Login(LoginQuery login);
        NotifyInvalid AlterarGrupo(UsuarioGrupoCommand usuarioGrupoCommand);
    }
}
