﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.Domain.Usuarios.ValueObjects;

namespace TodoApi.Domain.Tarefas.Entities
{
    public class Usuario
    {
        public string Id { get; private set; }
        public NomeCompleto Nome { get; private set; }
        public string IdEquipe { get; private set; }
    }
}
