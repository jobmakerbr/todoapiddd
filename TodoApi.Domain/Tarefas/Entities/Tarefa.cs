﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.CrossCutting;

namespace TodoApi.Domain.Tarefas.Entities
{
    public enum enumStatusTarefa
    {
        Criada,
        Completada
    }

    public enum enumTipoTarefa
    {
        Usuario,
        Equipe
    }

    public class Tarefa
    {
        public Tarefa(string descricao, DateTime? dhCompletada, enumStatusTarefa status, enumTipoTarefa tipo, Usuario usuario)
        {
            this.Descricao = descricao;
            this.DhCompletada = dhCompletada;
            this.StatusTarefa = status;
            this.TipoTarefa = tipo;
            this.UsuarioCriador = usuario;
        }

        public Tarefa(string descricao, DateTime? dhCompletada, enumStatusTarefa status, enumTipoTarefa tipo, Usuario usuario, EquipeUsuarios equipe)
        {
            this.Descricao = descricao;
            this.DhCompletada = dhCompletada;
            this.StatusTarefa = status;
            this.TipoTarefa = tipo;
            this.UsuarioCriador = usuario;
            this.Equipe = equipe;
        }

        public string Id { get; set; }
        public string Descricao { get; set; }
        public DateTime DhCriada { get; set; }
        public DateTime? DhCompletada { get; set; }
        public enumStatusTarefa StatusTarefa { get; set; }
        public enumTipoTarefa TipoTarefa { get; set; }
        public Usuario UsuarioCriador { get; set; }
        public EquipeUsuarios Equipe { get; set; }

        public NotifyInvalid Validate()
        {
            var notify = new NotifyInvalid();
            if (this.TipoTarefa == enumTipoTarefa.Equipe && this.Equipe == null) notify.AddMessage("Uma tarefa de equipe deve ter um equipe informada.");
            if (this.UsuarioCriador == null) notify.AddMessage("Uma tarefa deve ter um usuário criador.");
            return notify;
        }
    }
}
