﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApi.Domain.Tarefas.Entities
{
    public class EquipeUsuarios
    {
        public EquipeUsuarios(string nome)
        {
            this.Nome = nome;
        }

        public string Id { get; private set; }
        public string Nome { get; private set; }
        public List<Usuario> Usuarios { get; private set; }
    }
}
