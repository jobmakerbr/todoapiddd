﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApi.Domain.Tarefas.Dtos
{
    public class TarefaEquipeCommand
    {
        public string IdUsuario { get; set; }
        public string IdNovoGrupo { get; set; }
    }
}
