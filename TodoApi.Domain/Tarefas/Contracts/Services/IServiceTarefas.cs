﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoApi.CrossCutting;
using TodoApi.Domain.Tarefas.Dtos;

namespace TodoApi.Domain.Tarefas.Contracts.Services
{
    public interface IServiceTarefas
    {
        NotifyInvalid AlterarTarefas(TarefaEquipeCommand command); 
    }
}
